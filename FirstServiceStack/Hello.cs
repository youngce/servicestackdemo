﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;


namespace FirstServiceStack
{
    // the Request DTO
    [Route("/hello")]
    [Route("/hello/{Name}")]
    public class Hello
    {
        public string Name { get; set; } 
    }
    
    //Response DTO
    public class HelloResponse
    {
        public string Result { get; set; }
    }
    
    
    //Web Service implementation
    public class HelloService : Service
    {
        public object Any(Hello request)
        {
            return new HelloResponse { Result = "Hello, " + request.Name };
        }
    } 



}